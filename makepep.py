#! python3

import argparse, os, sys, re, shutil, time

SCRIPT_NAME = os.path.split(__file__)[1]

get_imports_as = re.compile(r"(?<=\bas\b)(\s*\w*,)*(\s*\w*)")
get_class_name = re.compile(r"(?<=\bclass\b)\s*\w*(?=\()")
split_class_name = re.compile(r'([A-Z_][a-z0-9]*)')
get_func_name = re.compile(r"(?<=\bdef\b)\s*\w*(?=\()")
get_variable_names  = re.compile(r"(\s*[a-zA-z0-9._]*,)*\s*([a-zA-z0-9._]*)\s*(?==[^=])")

IN_NAME_SEP = (
    '_',
    '-'
)

# Dict containing all changed names with original names as key and formatted names as value.
changed_names = {}

class TStopWatch():
    def __init__(self, process_name, out=sys.stdout, prefix='') -> None:
        self.process_name = process_name
        self.out = out
        self.prefix = prefix


    def __enter__(self):
        self.start_time = time.time()
        print(f'{self.prefix}Starting --> {self.process_name}.', file=self.out)
        return self


    def get_time(self) -> int:
        self.stop_time = time.time()
        return self.stop_time - self.start_time


    def __exit__(self, type, value, traceback):
        time_elapsed = self.get_time()
        print(f'{self.prefix}{self.process_name} finished in {time_elapsed:.2f} seconds.', file=self.out)


def get_total_line_count(full_path) -> int:
    # Opens file for reading in binary,  and counts nr of new lines.
    with open(full_path, 'rb', buffering=0) as file:
        lines = 0
        buffer_size = 1024 * 1024
        
        buffer_ = file.read(buffer_size)
        while buffer_:
            lines += buffer_.count(b'\n')
            buffer_ = file.read(buffer_size)

    return lines


def to_low_under(string) -> str:
    # Formats a string to lower_case_with_underscore.
    new_str = ''
    for i in range(len(string)):
        if string[i].isupper():
            if i == 0 or string[i - 1] in IN_NAME_SEP:
                # make lower
                new_str += string[i].lower()
            else:
                # make lower and add _ behind
                new_str += (IN_NAME_SEP[0] + string[i].lower())
        else:
            new_str += string[i]
    return new_str


def to_cap_words(string) -> str:
    # Formats a class name to CapWords and returns the formatted string.
    return ''.join([e.strip('_').title() for e in split_class_name.split(string) if len(e.strip('_').title()) > 0])


def find_special_names(line, regex_object) -> list:
    '''
    Performs a regex search with the specified RegexObject,
    replaces all commas (',') with a space and splitts the string.
    Returns a list of all comma or space separated names found.
    '''
    try:
        match_object = regex_object.search(line)
        special_names = (str(match_object.group())).replace(',', ' ').split()
    except AttributeError:
        special_names = []
    return special_names


def add_special_names_to_dict(name_list, dict_, cap=False) -> None:
    '''
    Checks if a name is saved in dict and if it should be.
    If not saved and should be it formats and saves key value pair to dict.
    If cap is True, formats with to_cap_words function (for class name formatting),
    else formats with to_low_under function.
    '''
    for name in name_list:
        if cap and not name in dict_:
            new_name = to_cap_words(name)
            if name != new_name:
                dict_[name] = new_name
        else:
            name = name.split('.')[-1]
            if not (name.islower() or name.isupper()) and not name in dict_:
                new_name = to_low_under(name)
                dict_[name] = new_name


def format_line(line) -> str:
    '''
    Finds function definitions and variable declarations.
    All unique names are saved to global dict changed_names.
    Finally it formats the string and returns the result.
    '''
    imports_as = find_special_names(line, get_imports_as)
    add_special_names_to_dict(imports_as, changed_names)

    class_name = find_special_names(line, get_class_name)
    add_special_names_to_dict(class_name, changed_names, cap=True)

    func_name = find_special_names(line, get_func_name)
    add_special_names_to_dict(func_name, changed_names)

    var_names = find_special_names(line, get_variable_names)
    add_special_names_to_dict(var_names, changed_names)

    # Format line, find matches from dict and replace with dict values.
    # Sort dict to longest values first to avoid formatting alike substrings with the wrong key.
    for key in sorted(changed_names, key=lambda key: len(changed_names[key]), reverse=True):
        line = line.replace(key, changed_names[key])

    return line


def progress_bar(total, progress=0, out=sys.stdout, prefix='') -> None:
    percent =f'{100 * (progress / total):.2f}'
    prefix_ = f'{prefix}Progress: ['
    suffix_ = f']{percent:>7}% Completed. '

    len_ = shutil.get_terminal_size()[0] -1 - len(prefix_) - len(suffix_)
    filled_len = int(len_ * progress / total)
    bar = ('#' * filled_len) + ('-' * (len_ - filled_len))

    print(f'\r{prefix_}{bar}{suffix_}', file=out, end='', flush=True)


def create_arg_parser() -> argparse.ArgumentParser:
    '''Returns an argparse object with certain opitions.'''
    parser = argparse.ArgumentParser(description=(''
        'A simple script to convert a code document from mixed '
        'case to the PEP standard with lower_case_under_score.'
        ))

    parser.add_argument('source', help='The path to the source file. Enter TEST to run testfile (Note: testFile.py must be present in current dir).')
    parser.add_argument('-d', '--dest', help='Optional path to destination file (do not overwrite original file).', default=None)
    
    return parser


def main():
    with TStopWatch(f'File formatting with {SCRIPT_NAME}') as main_stopwatch:
        # Get command line aruments and set file variabes.
        arg_parser = create_arg_parser()
        parsed_args = arg_parser.parse_args(sys.argv[1:])

        if parsed_args.source == 'TEST':
            source_file = os.path.abspath('testFile.py')
            temp_file = source_file + '.tmp'
            dest_file = None
        else:
            source_file = os.path.abspath(parsed_args.source)
            dest_file = os.path.abspath(parsed_args.dest) if parsed_args.dest is not None else source_file
            temp_file = source_file + '.tmp'

        # Calculate nr of lines
        total_lines = get_total_line_count(source_file)

        # Print info to stdout
        align_width = 15
        info_lables = ['Source file', 'Dest. file', 'Nr of lines']
        print()
        print(f'{info_lables[0]:<{align_width}}: {source_file}')
        print(f'{info_lables[1]:<{align_width}}: {dest_file if dest_file is not None else temp_file}')
        print(f'{info_lables[2]:<{align_width}}: {total_lines}')
        print()
        
        # Read in-file, format and write to temp-file.
        progress = 0
        with open(source_file, 'r', encoding='utf-8') as in_file:
            with open(temp_file, 'w', encoding='utf-8') as tmp_file:
                for line in in_file:
                    tmp_file.write(format_line(line))
                    progress_bar(total_lines, progress)
                    progress += 1
        print()

        # Save to dest (rename temp-file to dest-file).
        print(f'\nSaving result to destination file.')
        if dest_file is not None:
            shutil.move(temp_file, dest_file)
        

if __name__ == '__main__':
    main()

#! python3

'''
This is a test file for formatToPEP.py
It might or might not work on it's own (i.e. be a runnable python script).
Feel free to change the contents of this file to match your test needs!
'''

import random, os, sys, time
from typing import NewType
from math import pow as mathPow

class HelloThere():
    """docstring for HelloThere"""
    def __init__(self, arg):
        self.arg = arg
        self.otherArg = None

    def printHello(self):
        print('HELLO ' * self.arg)
        

def getRandom_Int() -> int:
    return random.randint(0, 30)


def printTime():
    print(time.asctime())


def printTimeAgain(time):
    print(f'Time is currently: {time}')


getRandom_Int()
printTime()
printTimeAgain('12:14 thu 23 Dec')


class newClass__Name():
    def __init__(self, arg):
        self.arg = arg


def other_func():
    print("I'm OK!")

''' 
This
    is a multiline
comment
        see printHello in HelloThere
'''

varIable = 12

# notVariable

otherVar = mathPow(2,3)

messageMessage = HelloThere(2)
messageMessage.printHello()

messageMessage.otherArg = 67
print(messageMessage.otherArg)

# this is a comment
# see printTimeAgain() for timeAgain

third_Var = newClass__Name(3)

